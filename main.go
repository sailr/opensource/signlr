package main

import (
	"fmt"
	"gitlab.com/sailr/skunkworks/signaller/cmd/signaller"
	"gitlab.com/sailr/skunkworks/signaller/internal/db"
	"gitlab.com/sailr/skunkworks/signaller/internal/server"
	"gitlab.com/sailr/skunkworks/signaller/pkg/license"
)

func main() {
	go signaller.Start()
	go db.Connect()
	go fmt.Println(license.Generate(""))
	server.Start()
}
