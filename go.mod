module gitlab.com/sailr/skunkworks/signaller

go 1.13

require (
	github.com/go-pg/pg/v9 v9.0.0
	github.com/gorilla/mux v1.7.3
)
