# Signlr

## Overview

Signlr provides two license types:

Short - 8 characters
Regular - 32 characters

Signlr uses two methods of hashing to ensure key authenticity, uniqueness, and validity.

## Hashing

We use Golang's internal hashing mechanisms to create hashes based on a randomly generated string. That string is then stored into the database and available for validation



## Terminology

* license - a randomly generated string consisting of numbers and characters of a predetermined length (8 or 32 characters)
*
