
## 0.0.5 [11-10-2019]

* Bug fixes and performance improvements

See commit 50ab0da

---

## 0.0.4 [11-09-2019]

* Bug fixes and performance improvements

See commit 0771aa5

---

## 0.0.3 [11-04-2019]

* Bug fixes and performance improvements

See commit 1923800

---

## 0.0.2 [11-04-2019]

* Bug fixes and performance improvements

See commit d5e7735

---
