package db

import (
	"fmt"
	"github.com/go-pg/pg/v9"
)

// Contact represents a Contact model in the database
type Contact struct {
	ID        int
	Name      string
	Phone     string
	Address   string
	CreatedAt string `db:"created_at"`
	UpdatedAt string `db:"updated_at"`
}

// Connect returns a connection to the database
func Connect() *pg.DB {
	db := pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "",
		Database: "jamesonstone",
	})
	defer db.Close()
	contact := &Contact{
		ID: 11,
	}
	err := db.Select(contact)
	if err != nil {
		panic(err)
	}
	fmt.Println(contact)
	return db
}
