package server

import (
	"fmt"
	"html"
	"net/http"
	"log"
	"github.com/gorilla/mux"

	"gitlab.com/sailr/skunkworks/signaller/pkg/license"
)

// Start server
func Start() {
		r := mux.NewRouter()

		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello, World")
		})
		r.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
		})
		r.HandleFunc("/validate", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, r.Method)
		})
		r.HandleFunc("/create", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, license.Generate(""))
		})

		// Start the server with the router
		log.Print("Listening at :3030")
		log.Fatal(http.ListenAndServe(":3030", r))
}
