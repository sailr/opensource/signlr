FROM golang:alpine AS build-stage
WORKDIR /signlr
RUN apk add --update git
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o signlr /signlr

FROM scratch
COPY --from=build-stage /signlr/signlr .
CMD ["./signlr"]
