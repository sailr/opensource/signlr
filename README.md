# Signlr

Signlr CVUD (Create Validate Update Delete) key-license management platform.

## Overview

Signlr aims to provide a platform for quickly generating license keys, and performing CRUD operations (including validation) on them. This solution can be hosted locally or by us at `signlr.sailr.co`.

## Installing

Signlr can be run as a container image or binary.

## Getting Started

`docker run -p 3030:3030 signlr:latest`

to verify:
