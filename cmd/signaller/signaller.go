package signaller


import (
	"fmt"
)

// Start a thing
func Start() {
	fmt.Println("Hello, World")
}
