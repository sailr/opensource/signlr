package license

import(
	"strings"
	"time"
	"math/rand"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// Generate generates a hash based on a random string and returns it
func Generate(length string) string {
	var l int
	if length == "short" {
		l = 8
	} else {
		l = 32
	}
	generatedKey := generateKey(l)
	return generatedKey
}

// Verify Verifies the syntactical constraints of the key
func Verify(currentKey string) bool{
	charList := strings.Split(currentKey, "")
	if len(charList) != 8 || len(charList) != 32 {
		return false
	}
	for _, v := range charList {
		if !strings.Contains(string(letters), v){
			return false
		}
	}
	return true
}

// Update changes the duration of the key's validity
func Update() {}

// Delete removes a key in teh database
func Delete() {}

// generatekey generates a valid key
func generateKey(length int) string {
	rand.Seed(time.Now().UnixNano())
	k := make([]rune, length)
	for i := range k {
			k[i] = letters[rand.Intn(len(letters))]
	}
	return string(k)
}
